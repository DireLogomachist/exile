# EXILE #
Awaken your inner eye and cut down your foes in an ascii art adventure

### Controls ###
Keys - type commands  
Enter - submit commands  
F11 - toggle windowed/fullscreen  
Escape - quit  

### Tools ###
Made in Python with Tkinter  
Written in Sublime Text 3  
Sound effects via soundfxcenter.com, freesfx.co.uk, freesound.org, and zapsplat.com

### Credits ###
[@andy_codes](https://twitter.com/andy_codes)
