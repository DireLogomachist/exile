SCROLLING TEXT USER STORY
==========================
NEED
	Scrolling text functionality

SOLUTION
	ScrollingText object that can be called and return text on a frame loop
	
	Will take a time elapsed (frame time) input and return the correct partial or complete string depending its scroll_speed attribute		



ATTRIBUTE NEEDS
	lifetime_cntr
		Holds lifetime counter that gets added to on every frame
	full_text
		Holds the full string given to it on creation
	text
		Holds the up-to-date partial string to be written to a TK text box
	scroll_speed
		Holds the speed on which to add to the partial text on update()
	changed
		Holds the bool value indicated whether the text variable changed in the last update()

METHOD NEEDS
	update(frametime)
		Calculates the new partial text to be held in the 'text' variable and sets the 'changed' variable if updated in the last update cycle