

from src.core.display import Display
from src.core.sound import Sound
from src.core.manager import Manager
from src.core.level import Level
from levels.level_1 import content as level_1
from levels.level_2 import content as level_2
from levels.level_3 import content as level_3


if __name__ == '__main__':
	
	manager = Manager.Instance()
	sound = Sound()
	display = Display()
	levels = [Level(level_1), Level(level_2), Level(level_3)]
	manager.start(display, sound, levels)

	display.tk.mainloop()
