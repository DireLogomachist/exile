# -*- mode: python -*-

block_cipher = None


a = Analysis(['exile.py'],
             pathex=['C:\\Repos\\EXILE'],
             binaries=[],
             datas=[
              ('audio/environment', 'audio/environment'),
              ('audio/interaction', 'audio/interaction'),
              ('art', 'art'),
              ('levels', 'levels')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='exile',
          debug=False,
          strip=False,
          upx=True,
          console=False,
          icon='art/icon.ico' )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='exile')
