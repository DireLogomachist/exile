

from src.gameobj.rainstorm import Rainstorm
from src.gameobj.raindrop import Raindrop
from src.gameobj.sprite import Sprite
from src.gameobj.scrolling_text import ScrollingText
from src.gameobj.fade import Fade
from src.core.manager import Manager


def intro():
	s = Sprite(['EXILE/art/ui/title_alpha.txt'], pos = [22,5], vel = [0,0])
	Manager.Instance().display.disable_input()
	Manager.Instance().display.canvas.configure(fg='black')
	Manager.Instance().display.textbox.configure(fg='black')
	Manager.Instance().display.textdisplay.configure(fg='black')

def intro_prompt():
	s = Sprite(['EXILE/art/ui/enter_prompt.txt'], pos = [26,13], vel = [0,0])

def scene_1_start():
	Manager.Instance().sprite_buffer = []
	Manager.Instance().display.disable_input()
	Manager.Instance().sound.master_volume = 0.5

def scene_1_chapter():
	Manager.Instance().sprite_buffer = []
	Sprite(['EXILE/art/ui/chapter_1.txt'], pos = [30,9])

def scene_1_part_1():
	# Play background sounds
	Manager.Instance().sprite_buffer = []
	tk = Manager.Instance().display.tk
	sound = Manager.Instance().sound
	tk.after(0, lambda: sound.play('audio/environment/breeze_fade_in.wav', 1.5))
	tk.after(4410, lambda: sound.play_loop('audio/environment/breeze.wav', 1.5, 'breeze'))
	tk.after(7000, lambda: sound.play('audio/environment/crow.wav', .1))

def scene_1_part_2():
	# Play sounds of approaching footsteps
	tk = Manager.Instance().display.tk
	sound = Manager.Instance().sound
	tk.after(0, lambda: sound.play('audio/environment/traveler_walking_fade_in.wav'))

def scene_1_part_3():
	# Play sounds of passing footsteps
	tk = Manager.Instance().display.tk
	sound = Manager.Instance().sound
	tk.after(4000, lambda: sound.play('audio/environment/coindrop.wav'))
	tk.after(7690, lambda: sound.play('audio/environment/traveler_walking_fade_out.wav'))

def scene_take_coin():
	ScrollingText('No point in letting it go to waste. He picks it up.')
	Manager.Instance().choice_flags['coin'] = True

def scene_leave_coin():
	ScrollingText('A beggar he is not. The coin stays where it lies.')
	Manager.Instance().choice_flags['coin'] = False

def scene_1_part_4():
	# Play sounds of passing caravan
	sound = Manager.Instance().sound
	sound.play('audio/environment/caravan.wav', .4)

def scene_1_choice_1():
	Manager.Instance().choice_flags['1'] = 'insult'
	ScrollingText('"They don\'t wait for crones either. Be off.", he says.')

def scene_1_choice_2():
	Manager.Instance().choice_flags['1'] = 'question'
	ScrollingText('"This is a dangerous road for one like yourself. Why bother with me?", he says.')

def scene_1_part_5():
	if Manager.Instance().choice_flags['1'] == 'insult':
		ScrollingText('She replies, "Curses come easy to those who bear them already."')
	elif Manager.Instance().choice_flags['1'] == 'question':
		ScrollingText('She replies, "You look every bit a beggar, but I am not fooled."')

def scene_1_part_6():
	# Play wind sounds
	tk = Manager.Instance().display.tk
	sound = Manager.Instance().sound
	tk.after(1000, lambda: sound.play('audio/environment/wind_fade_in.wav', .05))
	tk.after(5000, lambda: sound.stop_loop('breeze'))
	tk.after(5000, lambda: sound.play_loop('audio/environment/wind.wav', .05))
	tk.after(7000, lambda: sound.play('audio/environment/thunder.wav', .05))

def scene_1_part_7():
	tk = Manager.Instance().display.tk
	sound = Manager.Instance().sound
	tk.after(500, lambda: Raindrop('|', [40, 0], [0, .03], sound=True))
	tk.after(1500, lambda: Raindrop('|', [20, 0], [0, .03], sound=True))
	tk.after(2200, lambda: Raindrop('|', [50, 0], [0, .03], sound=True))
	tk.after(2700, lambda: Raindrop('|', [28, 0], [0, .03], sound=True))
	tk.after(3000, lambda: Raindrop('|', [34, 0], [0, .03], sound=True))
	tk.after(3100, lambda: Raindrop('|', [44, 0], [0, .03], sound=True))
	tk.after(3500, lambda: Raindrop('|', [18, 0], [0, .03], sound=True))
	tk.after(3800, lambda: Raindrop('|', [64, 0], [0, .03], sound=True))
	tk.after(4000, lambda: Raindrop('|', [34, 0], [0, .03], sound=True))
	tk.after(4200, lambda: Rainstorm())

	tk.after(4500, lambda: sound.stop_loop())
	tk.after(4500, lambda: sound.play('audio/environment/wind_fade_out.wav', .05))
	tk.after(4500, lambda: sound.play('audio/environment/rainstorm_fade_in.wav'))
	tk.after(4500, lambda: sound.play('audio/environment/raindrops_fade_out.wav', .3))
	tk.after(10280, lambda: sound.play_loop('audio/environment/rainstorm.wav'))

def scene_1_part_8():
	tk = Manager.Instance().display.tk
	tk.after(1200, lambda: Sprite(['EXILE/art/character/mystic.txt'], pos = [31, 5], vel = [0,0], tag='mystic'))

def scene_1_part_9():
	tk = Manager.Instance().display.tk

	ls = Sprite(['EXILE/art/environment/lightning_2.txt'], pos = [48,0], vel = [0,0])
	ls.disable()
	tk.after(3100, lambda: ls.enable())
	tk.after(3600, lambda: Manager.Instance().remove_sprite_by_tag('mystic'))
	tk.after(3700, lambda: Manager.Instance().remove_sprite(ls))
	tk.after(2800, lambda: Manager.Instance().sound.play('audio/environment/thunder.wav', 1))

def scene_1_end():
	Manager.Instance().object_buffer = []
	Manager.Instance().sound.play('audio/environment/rainstorm_fade_out.wav')
	Manager.Instance().sound.stop_loop()
	#s = Sprite(['EXILE/art/ui/title_demo_0.1.0.txt'], pos = [22,5], vel = [0,0])


content = [
	intro, 'wait 1',
	lambda: Fade('in', 2), 'wait 3',
	intro_prompt,'wait 1',
	lambda: ScrollingText(' '),
	lambda: Fade('out', 1), 'wait 2',
	scene_1_chapter,
	lambda: Fade('in', 1), 'wait 2',
	lambda: Fade('out', 1.5), 'wait 2',
	scene_1_start,
	lambda: Fade('in', 1, 'all'), 'wait 2',
	scene_1_part_1,
	lambda: ScrollingText('A blind man sits upon the bare earth.'),
	lambda: ScrollingText('He is an #exile#. A man with neither home nor country.'),
	lambda: ScrollingText('Resting in the shade of a palm tree, he waits.'),
	lambda: ScrollingText('He adjusts the sword in his lap and listens to the sounds of the road.'),
	scene_1_part_2,
	lambda: ScrollingText('A traveler approaches.'),
	scene_1_part_3,
	lambda: ScrollingText('A Bedouin man, from the sound of the it. He pauses and tosses a coin into\nthe dirt.'),
	lambda: ScrollingText('Does he #take# the coin or #leave# it?', mode='input'),
	{
		('take', 'take it', 'take coin'): lambda: scene_take_coin(),
		('leave', 'leave it', 'leave coin'): lambda: scene_leave_coin()
	},
	scene_1_part_4,
	lambda: ScrollingText('A caravan comes after him. Carts piled high with spices and creaking with goods.'),
	lambda: ScrollingText('They do not stop. Merchants have no use for strangers with empty pockets.'),
	lambda: ScrollingText('Another follows the caravan from a short distance. An old woman from the\nsound of her weak steps.'),
	lambda: ScrollingText('She pauses as the caravan continues.'),
	lambda: ScrollingText('"Best find shelter before the rain starts. The storm waits for no man."'),
	lambda: ScrollingText('The exile hangs his head as he decides his response.'),
	lambda: ScrollingText('Does he #insult# or #question# her?', mode='input'),
	{
		('insult', 'insult her'): lambda: scene_1_choice_1(),
		('question', 'question her'): lambda: scene_1_choice_2()
	},
	scene_1_part_5,
	lambda: ScrollingText('"I have a gift for you. One fit for an #exile#.", she whispers.'),
	lambda: ScrollingText('"You failed in your duty and lost your eyes. I shall return them to you."'),
	lambda: ScrollingText('"The #True Nazar#, your inner eye. You shall see more than you\never wished you could."'),
	lambda: ScrollingText('Before he could move, her hand is against his forehead. He falls back as a\nsplitting pain tears through his skull.'),
	lambda: ScrollingText('The exile scrambles to his feet. He clutches his sheathed sword and\ndemands answers.'),
	lambda: ScrollingText('She only says...'), 
	lambda: ScrollingText('"#Listen#"'),
	scene_1_part_6,
	lambda: ScrollingText('He turns his head to the sky.'),
	'wait 1',
	scene_1_part_7,
	'wait 8',
	lambda: ScrollingText('His #inner eye# opens as he stares into the storm.'),
	scene_1_part_8,
	lambda: ScrollingText('The woman appears in the rain.'),
	lambda: ScrollingText('"This #sight# is a gift. Do not misuse it."'),
	scene_1_part_9,
	lambda: ScrollingText('A flash! ...and she is gone once again.'),
	'wait 1',
	lambda: Fade('out', 3),
	'wait 4',
	scene_1_end,
	#lambda: Fade('in', 3),
	'wait 4',
	#lambda: Sprite(['EXILE/art/ui/exit_prompt.txt'], pos = [24,13], vel = [0,0])
]
