

from src.gameobj.sprite import Sprite
from src.gameobj.scrolling_text import ScrollingText
from src.gameobj.fade import Fade
from src.gameobj.combat_intro import CombatIntro
from src.gameobj.guard_fight import GuardFight
from src.gameobj.remnant import Remnant
from src.core.manager import Manager


def scene_start():
	Manager.Instance().sprite_buffer = []
	Manager.Instance().display.disable_input()
	Manager.Instance().display.canvas.configure(fg='black')
	Manager.Instance().display.textbox.configure(fg='#cccccc')
	Manager.Instance().display.textdisplay.configure(fg='#cccccc')

def scene_chapter():
	Sprite(['EXILE/art/ui/chapter_2.txt'], pos = [30,9])

def scene_part_1():
	Manager.Instance().sprite_buffer = []
	Sprite(['EXILE/art/environment/city.txt'], pos = [0,1])
	Manager.Instance().sound.play('audio/environment/breeze_fade_in.wav', .6)
	tk = Manager.Instance().display.tk
	tk.after(4410, lambda: Manager.Instance().sound.play_loop('audio/environment/breeze.wav', .6))

def scene_part_2():
	Manager.Instance().sprite_buffer = []
	Sprite(['EXILE/art/environment/gate.txt'], pos = [0,2])

def scene_part_3():
	Manager.Instance().sprite_buffer = []
	Sprite(['EXILE/art/environment/smugglersgate.txt'], pos = [-25, -2])
	Manager.Instance().sound.stop_loop()

def scene_part_4():
	Manager.Instance().sprite_buffer = []
	Sprite(['EXILE/art/environment/slums.txt'], pos = [2,1])
	Manager.Instance().sound.play_loop('audio/environment/city.wav', .5)

def scene_part_5():
	Sprite(['EXILE/art/character/guard_alpha.txt','EXILE/art/character/guard_sword_drawn_alpha.txt'], pos=[32,5], vel=[0,0], tag='guard')

def scene_bribe_attempt():
	if Manager.Instance().choice_flags.get('coin'):
		ScrollingText('The #exile# draws a single coin from his pocket and offers it.')
		ScrollingText('The #guard# laughs and draws his weapon.')
	else:
		ScrollingText('The #exile# digs in his pocket and finds it empty.')
		ScrollingText('The #guard# laughs and draws his weapon.')

def scene_part_6():
	Manager.Instance().clear_buffers()
	Sprite(['EXILE/art/character/guard_body.txt'], pos = [20, 11])

def scene_part_7():
	Manager.Instance().display.tk.after(1500, lambda: Remnant([37,4]))

def scene_part_8():
	Manager.Instance().remove_sprite_by_tag('remnant')

def scene_end():
	Manager.Instance().clear_buffers()
	Sprite(['EXILE/art/ui/title_demo_0.2.0.txt'], pos = [22,5], vel = [0,0])


content = [
	scene_start,
	scene_chapter,
	lambda: Fade('in', 2), 'wait 3',
	lambda: Fade('out', 1.5), 'wait 2',
	scene_part_1,
	lambda: Fade('in', 1.5), 'wait 2',
	lambda: ScrollingText('Palmyra.'),
	lambda: ScrollingText('A merchant city of tens of thousands.'),
	lambda: ScrollingText('The #exile# has history here, as do many.'),
	lambda: Fade('out', 1.5), 'wait 2',
	scene_part_2,
	lambda: Fade('in', 1.5), 'wait 1',
	lambda: ScrollingText('He approaches the gate.'),
	lambda: ScrollingText('It is guarded by the #Queen\'s Legion#.'),
	lambda: ScrollingText('The walls are high and the guards ask questions, but the #exile# knows\nanother way in.'),
	lambda: Fade('out', 1.5), 'wait 2',
	scene_part_3,
	lambda: Fade('in', 1.5), 'wait 2',
	lambda: ScrollingText('#The Smuggler\'s Gate#.'),
	lambda: ScrollingText('Many a trader uses it to avoid the Queen\'s taxes.'),
	lambda: ScrollingText('He listens for the patrol and then enters. '),
	lambda: Manager.Instance().sound.play('audio/environment/steps_echo.wav', .5),
	lambda: Fade('out', 1.5), 'wait 2',
	scene_part_4,
	lambda: Fade('in', 1.5), 'wait 2',
	lambda: ScrollingText('The city proper. He emerges near the eastern slums.'),
	lambda: ScrollingText(u'He steps forward\u2014'),
	scene_part_5, 'wait 2',
	lambda: ScrollingText('A #guard#!'),
	lambda: ScrollingText('"Halt!" The #guard# yells. "What is your business here?"'),
	lambda: ScrollingText('Should he #fight# the guard or attempt to #bribe# him?', mode='input'),
	{
		('fight', 'fight him'): lambda: ScrollingText('The #exile# falls into a dueling stance as the #guard# draws his blade.'),
		('bribe', 'bribe him'): lambda: scene_bribe_attempt()
	},
	lambda: Manager.Instance().sprite_by_tag('guard').next_sprite_frame(),
	lambda: Manager.Instance().sound.play('audio/interaction/guard_draw.wav', .5),
	lambda: ScrollingText('The #exile# has no choice. He grips his sword tight.'),
	lambda: Fade('out', 1.5), 'wait 1.5',
	lambda: Manager.Instance().sound.stop_loop(),
	CombatIntro, 'wait 7',
	lambda: Sprite(['EXILE/art/ui/combat_prompt.txt'], pos = [13,6], vel = [0,0]),
	lambda: Fade('in', 1.5), 'wait 2',
	lambda: ScrollingText(' '),
	lambda: Fade('out', 1.5), 'wait 2',
	GuardFight,
	lambda: Fade('out', 1.5), 'wait 1.5',
	scene_part_6,
	lambda: Fade('in', 1.5), 'wait 2',
	lambda: ScrollingText('The guard falls to the ground.'),
	lambda: ScrollingText('The #exile# knows others may be on the way. He sheaths his blade and moves\nto leave.'),
	scene_part_7,
	lambda: ScrollingText(u'But wait\u2014'),
	lambda: ScrollingText('A wisp rises from the collapsed guard.'),
	lambda: ScrollingText('The #exile# curses. "It\'s spread even to the Legion."'),
	scene_part_8,
	lambda: ScrollingText('The wisp dissipates, leaving no trace in the air.'),
	lambda: ScrollingText('He turns and continues into the city.'),
	lambda: ScrollingText('He knows he must finish this quickly.'),
	lambda: Fade('out', 1.5), 'wait 2',
	scene_end,
	#lambda: Fade('in', 3),
	#'wait 4',
	#lambda: Sprite(['EXILE/art/ui/exit_prompt.txt'], pos = [24,13], vel = [0,0])
]
