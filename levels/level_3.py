

from src.gameobj.sprite import Sprite
from src.gameobj.particle import Particle
from src.gameobj.particle_cloud import ParticleCloud
from src.gameobj.altar_flame import AltarFlame
from src.gameobj.scrolling_text import ScrollingText
from src.gameobj.fade import Fade
from src.gameobj.combat_intro import CombatIntro
from src.gameobj.jinn_fight import JinnFight
from src.core.manager import Manager


def scene_start():
	Manager.Instance().clear_buffers()
	Manager.Instance().display.disable_input()
	Manager.Instance().display.canvas.configure(fg='black')
	Manager.Instance().display.textbox.configure(fg='#cccccc')
	Manager.Instance().display.textdisplay.configure(fg='#cccccc')

def scene_chapter():
	Manager.Instance().clear_buffers()
	Sprite(['EXILE/art/ui/chapter_3.txt'], pos = [30,9])

def scene_temple_steps():
	# Temple steps
	Manager.Instance().clear_buffers()
	pillar_l = Sprite(['EXILE/art/environment/pillar.txt'], [3,0])
	pillar_r = Sprite(['EXILE/art/environment/pillar.txt'], [70,0])
	temple = Sprite(['EXILE/art/environment/temple.txt'], [12,1])

def scene_temple_interior():
	# Temple interior
	Manager.Instance().clear_buffers()
	pillar_l = Sprite(['EXILE/art/environment/altar_pillar.txt'], [1,0])
	pillar_r = Sprite(['EXILE/art/environment/altar_pillar.txt'], [62,0])
	lamp_l = Sprite(['EXILE/art/environment/altar_lamp.txt'], [20,11])
	lamp_r = Sprite(['EXILE/art/environment/altar_lamp.txt'], [55,11])
	flame_l = AltarFlame([21,8])
	flame_r = AltarFlame([56,8])
	altar = Sprite(['EXILE/art/environment/altar.txt'], [30,13], [0,0], 'altar')
	Manager.Instance().sound.play_loop('audio/environment/altar_flames.wav', .5)

def scene_priest_appears():
	# Priest appears
	Manager.Instance().display.tk.after(1500, lambda: Fade('out', .5))
	Manager.Instance().display.tk.after(2000, lambda: Manager.Instance().remove_sprite_by_tag('altar'))
	Manager.Instance().display.tk.after(2000, lambda: Sprite(['EXILE/art/character/priest.txt','EXILE/art/character/jinn.txt'], [35,5], [0,0], 'priest'))
	Manager.Instance().display.tk.after(2100, lambda: Fade('in', .5))

def scene_priest_transforms():
	# Priest transforms
	cloud = ParticleCloud([40,7], 8, 4)

def scene_jinn_appears():
	# Jinn appears
	Manager.Instance().sprite_by_tag('priest').next_sprite_frame()

def scene_jinn_postfight():
	Manager.Instance().clear_buffers()
	Sprite(['EXILE/art/character/jinn.txt'], [33,5], [0,0], 'jinn')
	ParticleCloud([38,7], 8, 4)

def scene_jinn_collapse():
	Manager.Instance().display.tk.after(2000, lambda: Manager.Instance().remove_sprite_by_tag('jinn'))
	Manager.Instance().display.tk.after(2200, lambda: Manager.Instance().remove_object_by_tag('cloud'))
	Manager.Instance().display.tk.after(2000, lambda: Sprite(['EXILE/art/character/jinn_dead.txt'], [28,14], [0,0], 'jinn_corpse'))

def scene_jinn_death():	
	with open('../EXILE/art/vfx/sword_planted.txt') as f:
		text = f.read()
	Manager.Instance().display.tk.after(2000, lambda: Particle(text, [36,8],[0,0],0,'back'))
	Manager.Instance().display.tk.after(2000, lambda: Manager.Instance().sound.play('audio/interaction/combat_enemy_hit.wav', .2))

def scene_jinn_disappear():
	Manager.Instance().display.tk.after(2000, lambda: Manager.Instance().remove_sprite_by_tag('jinn_corpse'))

def scene_end():
	Manager.Instance().clear_buffers()
	Sprite(['EXILE/art/ui/title.txt'], pos = [22,3], vel = [0,0])
	Sprite(['EXILE/art/ui/credits.txt'], pos = [31,10], vel = [0,0])


content = [
	scene_start,
	scene_chapter,
	lambda: Fade('in', 2), 'wait 3',
	lambda: Fade('out', 1.5), 'wait 2',
	scene_temple_steps,
	lambda: Fade('in', 2), 'wait 3',
	lambda: ScrollingText('The Temple of Al-Lat.'),
	lambda: ScrollingText('Here was where it started.'),
	lambda: Fade('out', 1.5), 'wait 2',
	scene_temple_interior,
	lambda: Fade('in', 1.5), 'wait 2',
	lambda: ScrollingText('Empty.'),
	lambda: ScrollingText('This place was once filled with worshipers.'),
	lambda: ScrollingText('Prayers for safe travel and sacrifices for good harvests...'),
	lambda: ScrollingText('Something has driven it all away.'),
	scene_priest_appears,
	lambda: ScrollingText('The torches dim and the #priest# appears.'),
	lambda: ScrollingText('"Back once again? I thought I chased you off for good," the #priest# says.'),
	lambda: ScrollingText('Does he respond in #anger# or #resolution#?', mode='input'),
	{
		('anger', 'in anger'): lambda: ScrollingText('"You\'ll pay for what you\'ve done here," the #exile# growls.'),
		('resolution', 'in resolution'): lambda: ScrollingText('"This will all stop here," the #exile# says.'),
	},
	lambda: ScrollingText('The #priest# stands there for a moment...'),
	lambda: ScrollingText('"You\'re welcome to try," the #priest# smirks.'),
	scene_priest_transforms,
	lambda: ScrollingText('The #priest# begins to transform!'),
	scene_jinn_appears,
	lambda: ScrollingText('His form is revealed as a powerful #jinn#!'),
	lambda: ScrollingText('The #exile# draws his blade. This won\'t be easy.'),
	lambda: Fade('out', 1.5), 'wait 1.5',
	lambda: Manager.Instance().sound.stop_loop(),
	CombatIntro, 'wait 7',
	JinnFight,
	lambda: Fade('out', 1.5), 'wait 1.5',
	scene_jinn_postfight,
	lambda: Fade('in', 1.5), 'wait 2',
	scene_jinn_collapse,
	lambda: ScrollingText('The #jinn# shrieks and collapses.'),
	scene_jinn_death,
	lambda: ScrollingText('The #exile# finishes it off with a single strike.'),
	scene_jinn_disappear,
	lambda: ScrollingText('The last remnants of the #jinn# fade away.'),
	lambda: ScrollingText('And leaving his blade, the #exile# turns and limps away.'),
	lambda: Fade('out', 2), 'wait 3',
	scene_end,
	lambda: Fade('in', 2), 'wait 4',
	lambda: Sprite(['EXILE/art/ui/exit_prompt.txt'], pos = [24,12], vel = [0,0]),
]
