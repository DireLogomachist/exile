

from src.core.manager import Manager
import time
from math import sin, pi

class Cursor(object):
	def __init__(self, widget, text_color):
		self.widget = widget
		self.widget.configure(background='black', foreground=text_color, insertbackground=text_color)
		self.widget.configure(height=1, width=2, borderwidth=0)
		self.text_color = text_color

		self.states = {'disabled', 'enter', 'input'}
		self.state = 'disabled'
		self.current_char = ''
		self.sine_switch = False
		self.sine_start = 0
	

	def update(self, ftime):
		if self.state == 'enter':
			x = sin((time.clock() - self.sine_start)*2*pi)
			if x < 0 and self.sine_switch == False:
				self.widget.configure(foreground='black')
				self.sine_switch = True
			if x > 0 and self.sine_switch == True:
				self.widget.configure(foreground=self.text_color)
				self.sine_switch = False


	def set_state(self, state):
		if state in self.states and state != self.state:
			self.state_transition(state)
			self.state = state
		elif state not in self.states:
			raise Exception('Cursor: bad state change parameter \'' + state + '\'')

	def state_transition(self, state):
		if state == 'disabled':
			self.widget.configure(state='normal')
			self.widget.delete('1.0', 'end')
			self.widget.configure(state='disabled')
			self.current_char = ''
		elif state == 'enter':
			self.widget.configure(state='normal')
			self.widget.delete('1.0', 'end')
			self.widget.insert('1.0', u'\u25BC')
			self.widget.configure(state='disabled')
			self.current_char = u'\u25BC'
			self.sine_start = time.clock()
		elif state == 'input':
			self.widget.configure(state='normal', foreground=self.text_color)
			self.widget.delete('1.0', 'end')
			self.widget.insert('1.0', '>')
			self.widget.configure(state='disabled')
			self.current_char = '>'

	def on_wrong_input(self):
		if self.state == 'disabled' or self.state == 'enter':
			return
		self.widget.configure(state='normal')
		self.widget.configure(foreground='red')
		self.widget.delete('1.0', 'end')
		self.widget.insert('1.0', u'\u2715')
		self.widget.configure(state='disabled')
		Manager.Instance().display.tk.after(1000, self.reset_cursor)

	def reset_cursor(self):
		self.widget.configure(state='normal')
		self.widget.configure(foreground=self.text_color)
		self.widget.delete('1.0', 'end')
		self.widget.insert('1.0', self.current_char)
		self.widget.configure(state='disabled')
