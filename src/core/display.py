

from src.core.cursor import Cursor
from Tkinter import *
import tkFont
from itertools import chain
from src.lib.conversions import gen_pair, count_preceding_lines, count_preceding_line_chars


class Display(object):

	def __init__(self):
		self.tk = Tk()
		self.tk.wm_title('EXILE')
		self.tk.iconbitmap('art/icon.ico')
		self.tk.configure(background='black')

		self.canvas_height = 17
		self.canvas_width = 80
		self.textbox_height = 7
		self.textbox_width = 80
		self.fullscreen = False
		self.char_width = tkFont.Font(font='TkFixedFont').measure(' ')
		self.char_height = tkFont.Font(font='TkFixedFont').metrics('linespace')
		self.text_color = '#cccccc'

		self.frame = Frame(self.tk)
		self.frame.configure(background='green')
		
		self.canvas = Text(self.frame)
		self.canvas.configure(background='black', foreground=self.text_color)
		self.canvas.configure(height=self.canvas_height, width=self.canvas_width, borderwidth=0)
		self.canvas.configure(selectbackground='black', highlightcolor=self.text_color)
		with open('art/vfx/eye_canvas.txt') as f:
			self.canvas.insert(INSERT, str(f.read()))

		self.textbox = Text(self.frame)
		self.textbox.configure(background='black', foreground=self.text_color)
		self.textbox.configure(height=self.textbox_height, width=self.canvas_width, borderwidth=0)
		self.textbox.configure(selectbackground='black')
		self.textbase = ''
		with open('art/ui/outputframe.txt') as f:
			self.textbase = str(f.read())
			self.textbox.insert(INSERT, self.textbase)

		self.textdisplay = Text(self.textbox)
		self.textdisplay.configure(background='black', foreground=self.text_color)
		self.textdisplay.configure(height=2, width=self.canvas_width-6, borderwidth=0)
		self.textdisplay.configure(selectbackground='black')
		self.textdisplay.tag_config('green', foreground='green')
		self.textdisplay.tag_config('red', foreground='red')

		self.writebox = Text(self.textbox)
		self.writebox.configure(background='black', foreground=self.text_color, insertbackground=self.text_color)
		self.writebox.configure(height=1, width=self.textbox_width-7, borderwidth=0)
		self.writebox.configure(selectbackground='black')
		self.writebox.insert(INSERT, "")

		cursor_widget = Text(self.textbox)
		self.cursor = Cursor(cursor_widget, self.text_color)

		self.logbox = Text(self.textbox)
		self.logbox.configure(height=1, width=30, borderwidth=0, background='green')


		self.frame.pack()
		self.canvas.pack()
		self.textbox.pack(side=BOTTOM)
		self.textdisplay.place(x=3*self.char_width, y=3*self.char_height, anchor="w")
		self.cursor.widget.place(x=3*self.char_width,y=4*self.char_height+self.char_height/2, anchor='w')
		self.writebox.place(x=6*self.char_width, y=4*self.char_height+self.char_height/2, anchor="w")
		#self.logbox.place(x=50*self.char_width, y=6*self.char_height+self.char_height/2, anchor='w')
		self.textbox.configure(state=DISABLED)
		self.textdisplay.configure(state=DISABLED)
		self.canvas.configure(state=DISABLED)

		self.set_centered()
		self.set_fullscreen() if self.fullscreen else self.set_windowed()
		self.set_controls()
		self.tk.resizable(False, False)
		self.writebox.focus_set()


	def toggle_fullscreen(self):
		self.set_fullscreen() if not self.fullscreen else self.set_windowed()

	def set_fullscreen(self):
		self.tk.maxsize(self.tk.winfo_screenwidth(), self.tk.winfo_screenheight())
		self.tk.attributes('-fullscreen', True)
		self.frame.place(relx=.5, rely=.4, anchor="center")
		self.fullscreen = True

	def set_windowed(self):
		self.tk.attributes('-fullscreen', False)
		self.tk.minsize(642, 388)
		self.tk.maxsize(642, 388)
		self.frame.place(relx=.5, rely=.5, anchor="center")
		self.fullscreen = False

	def set_centered(self):
		w = self.char_width * (self.canvas_width) + 2
		h = self.char_height * (self.canvas_height + self.textbox_height) + 4
		ws = self.tk.winfo_screenwidth()
		hs = self.tk.winfo_screenheight()
		x = (ws/2) - (w/2)
		y = (hs/2) - (h/2) - 100

		self.tk.geometry('%dx%d+%d+%d' % (w, h, x, y))


	def enable_input(self):
		self.writebox.configure(state=NORMAL)
		self.cursor.set_state('input')

	def enable_input_enter(self):
		self.writebox.configure(state=DISABLED)
		self.cursor.set_state('enter')

	def disable_input(self):
		self.writebox.configure(state=DISABLED)
		self.cursor.set_state('disabled')

	def set_controls(self):
		self.remove_bindings(self.canvas, "Text")
		self.remove_bindings(self.textbox, "Text")
		self.remove_bindings(self.textdisplay, "Text")
		self.remove_bindings(self.logbox, "Text")
		self.remove_bindings(self.frame, "Frame")
		self.remove_bindings(self.cursor.widget, "Text")
		self.remove_bindings(self.tk, "Tk")

		self.tk.bind("<Escape>", lambda e: e.widget.quit())
		self.tk.bind("<F11>", lambda e: self.toggle_fullscreen())

	def get_events(self, widget):
		return str(set(chain.from_iterable(widget.bind_class(cls) for cls in widget.bindtags())))

	def remove_bindings(self, widget, name):
		bindtags = list(widget.bindtags())
		bindtags.remove(name)
		widget.bindtags(tuple(bindtags))

	def textbox_write(self, text):
		self.textbox.configure(state='normal')
		self.textdisplay.configure(state='normal')
		
		text, g_tags = self.extract_tags(text, '#')

		self.textdisplay.delete('1.0', 'end')
		self.textdisplay.insert('1.0', text)
		self.apply_tag('green', g_tags, text, self.textdisplay)
		self.textbox.configure(state='disabled')
		self.textdisplay.configure(state='disabled')

	def canvas_write(self, text):
		self.canvas.configure(state='normal')
		self.canvas.delete('1.0', 'end')
		self.canvas.insert('1.0', text)
		self.canvas.configure(state='disabled')

	def extract_tags(self, text, tag):
		indices = []
		split = list(text)

		for i, c in enumerate(split):
			if split[i] == tag:
				line = count_preceding_lines(text, i)
				indices.append([line, i - count_preceding_line_chars(text, i) - len([x for x in indices if x[0] == line]) - line + 1])
				split[i] = ''
		text = ''.join(split)
		return text, indices

	def apply_tag(self, tag, indices, text, widget):
		pairs = list(gen_pair(indices))
		for p in pairs:
			if len(p) != 2: p.append([p[0][0], str(len(text))])
			widget.tag_add(tag, str(p[0][0]) + '.' + str(p[0][1]), str(p[1][0]) + '.' + str(p[1][1]))
