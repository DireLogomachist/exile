

from src.core.manager import Manager
from collections import deque

class Level(object):
	def __init__(self, content = []):
		self.content = content
		self.content_index = 0
		self.timer = 0
		self.timer_goal = -1
		self.input_buffer = ''
		self.flag_queue = deque()
		self.fight_mode = False
		self.fight = None

		# When in fight mode:
		# 	- the content_index should not move forward after input or timer
		#	- the wait_for_input function needs to pass forward input to the fight loop,
		#		the input_buffer contents as well as call the fight's turn function


	def update(self, ftime):
		if self.content_index < len(self.content):
			if len(self.flag_queue) > 0:
				if self.flag_queue[0] == 'wait enter': self.wait_for_enter()
				elif self.flag_queue[0] == 'wait input': self.wait_for_input()
			elif self.fight_mode:
				return
			elif isinstance(self.content[self.content_index], dict):
				self.content_index += 1
			elif callable(self.content[self.content_index]):
				self.content[self.content_index]()
				self.content_index += 1
			elif self.content[self.content_index].split()[0] == 'wait' and self.timer_goal < 0:
				self.wait_seconds(float(self.content[self.content_index].split()[1]))

			if self.timer_goal > 0:
				self.timer += ftime
				if self.timer > self.timer_goal:
					self.content_index += 1
					self.timer_goal = -1
		else:
			Manager.Instance().load_next_level()


	def wait_for_enter(self):
		if self.input_buffer == '\r':
			Manager.Instance().text_buffer.popleft()
			Manager.Instance().display.textbox_write('')
			Manager.Instance().display.cursor.set_state('disabled')
			self.input_buffer = ''
			self.flag_queue.popleft()


	def wait_for_input(self):
		if self.input_buffer != '':
			self.input_buffer = self.input_buffer.lower()

			if self.fight_mode:
				self.fight.input_buffer = self.input_buffer
				self.fight.turn()
				Manager.Instance().text_buffer.popleft()
				Manager.Instance().display.textbox_write('')
				Manager.Instance().display.cursor.set_state('disabled')
				Manager.Instance().display.disable_input()
				self.flag_queue.popleft()
			else:
				found, func = self.find_input(self.content_index)
				if found:
					Manager.Instance().text_buffer.popleft()
					Manager.Instance().display.textbox_write('')
					Manager.Instance().display.cursor.set_state('disabled')
					Manager.Instance().display.disable_input()
					func()
					self.flag_queue.popleft()
					self.content_index += 1
				else:
					Manager.Instance().display.cursor.on_wrong_input()
			self.input_buffer = ''


	def wait_seconds(self, seconds):
		self.timer_goal = seconds * 1000
		self.timer = 0


	def find_input(self, index):
		for entry in self.content[index]:
			if self.input_buffer in entry:
				return True, self.content[index][entry]
		return False, None

	def clear_text(self):
		if len(self.flag_queue) > 0:
			Manager.Instance().text_buffer.popleft()
			Manager.Instance().display.textbox_write('')
			Manager.Instance().display.cursor.set_state('disabled')
			Manager.Instance().display.disable_input()
			self.flag_queue.popleft()

	def dump_text(self):
		Manager.Instance().text_buffer = deque()
		Manager.Instance().display.textbox_write('')
		Manager.Instance().display.cursor.set_state('disabled')
		Manager.Instance().display.disable_input()
		self.flag_queue = deque()
