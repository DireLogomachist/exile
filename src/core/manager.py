

from src.lib.singleton import Singleton
from src.lib.conversions import sprite_mask
import time
import sound
from collections import deque

@Singleton
class Manager(object):
	'Handles looping for all drawing and updates in the game'

	def __init__(self):
		self.sprite_buffer = []
		self.particle_buffer = []
		self.text_buffer = deque()
		self.object_buffer = []
		self.levels = []
		self.level_index = 0
		self.choice_flags = {}

		self.ftime = 0
		self.past_time = time.clock() * 1000
		self.schedule_time = 10


	def start(self, display, sound, levels=[]):
		self.display = display
		self.sound = sound
		self.levels = levels
		self.display.writebox.bind('<Return>', lambda e: self.submit_input())
		self.display.tk.after(self.schedule_time, self.update)

		self.display.tk.bind("1", lambda e: self.load_level(0))
		self.display.tk.bind("2", lambda e: self.load_level(1))
		self.display.tk.bind("3", lambda e: self.load_level(2))


	def update(self):
		'Calls updates on all objects in the sprite, particle, and text buffers and sets display object contents'
		start  = time.time()
		self.ftime = self.compute_ftime()
		
		for item in self.sprite_buffer:
			item.update(self.ftime)
		for item in self.particle_buffer:
			item.update(self.ftime)
		for item  in self.object_buffer:
			item.update(self.ftime)
		if len(self.text_buffer) > 0:
			self.text_buffer[0].update(self.ftime)
		if self.level_index < len(self.levels):
			self.levels[self.level_index].update(self.ftime)
		self.display.cursor.update(self.ftime)

		sprites = ''
		if len(self.sprite_buffer) > 0:
			for sprite in reversed(self.sprite_buffer):
				sprites = sprite_mask(sprite.to_canvas_space(), sprites)

		particles = ''
		if len(self.particle_buffer) > 0:
			for particle in reversed([x for x in self.particle_buffer if x.layer=='back']):
				particles = sprite_mask(particle.to_canvas_space(), particles)

		canvas = sprite_mask(particles, sprites)
		canvas = canvas.replace('A', ' ')

		particles = ''
		if len(self.particle_buffer) > 0:
			for particle in reversed([x for x in self.particle_buffer if x.layer=='fore']):
				particles = sprite_mask(particle.to_canvas_space(), particles)

		canvas = sprite_mask(canvas, particles)

		text = ''
		if len(self.text_buffer) > 0:
			text = self.text_buffer[0].text

		self.display.canvas_write(canvas)
		self.display.textbox_write(text)

		self.display.tk.after(self.schedule_time, self.update)
		end  = time.time()
		self.set_text(self.display.logbox, ' Update: ' + str("%03d" % (round((end-start)*1000, 2))) + ' ms\t\tPbuffer: ' + str(len(self.particle_buffer)))


	def compute_ftime(self):
		now = time.clock() * 1000
		delta = int(round(now - self.past_time))
		self.past_time = now
		return delta

	def register_sprite(self, sprite):
		self.sprite_buffer.append(sprite)

	def remove_sprite(self, sprite):
		if sprite in self.sprite_buffer:
			self.sprite_buffer.remove(sprite)

	def remove_sprite_by_tag(self, tag):
		self.sprite_buffer[:] = [x for x in self.sprite_buffer if x.tag != tag]

	def sprite_by_tag(self, tag):
		sprites = [x for x in self.sprite_buffer if x.tag == tag]
		if len(sprites) == 1: return sprites[0]
		else: return None

	def register_particle(self, particle):
		self.particle_buffer.append(particle)

	def remove_particle(self, particle):
		if particle in self.particle_buffer:
			self.particle_buffer.remove(particle)

	def register_text(self, text):
		self.text_buffer.append(text)

	def register_object(self, obj):
		self.object_buffer.append(obj)

	def remove_object(self, obj):
		if obj in self.object_buffer:
			self.object_buffer.remove(obj)

	def remove_object_by_tag(self, tag):
		self.object_buffer[:] = [x for x in self.object_buffer if x.tag != tag]

	def clear_buffers(self):
		self.sprite_buffer[:] = []
		self.particle_buffer[:] = []
		self.object_buffer[:] = []

	def set_text(self, widget, text):
		widget.delete('1.0', 'end')
		widget.insert('1.0', text)

	def submit_input(self):
		if self.level != None:
			if len(self.text_buffer) > 0 and self.text_buffer[0].mode == 'enter' and self.display.cursor.state != 'disabled':
				self.level.input_buffer = '\r'
			else:
				self.level.input_buffer = self.display.writebox.get(1.0, 'end').strip()
		self.set_text(self.display.writebox, '')

	@property
	def level(self):
		return self.levels[self.level_index]

	def load_next_level(self):
		if (self.level_index + 1) < len(self.levels):
			self.clear_buffers()
			self.level_index += 1

	def load_level(self, index):
		self.clear_buffers()
		self.level.clear_text()
		self.sound.stop_all_loops()
		self.level.content_index = 0
		if 0 <= index < len(self.levels):
			self.level_index = index
			self.level.content_index = 0
