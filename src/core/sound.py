

from pygame import mixer

class Sound(object):

	def __init__(self):
		mixer.pre_init(44100, -16, 2, 2048)
		mixer.init()
		mixer.set_num_channels(50)
		self.mixer = mixer
		self.master_volume = 1.0
		self.main_loop = None
		self.tagged_loops = {}

	def play(self, src, volume=1):
		s = mixer.Sound(src)
		s.set_volume(volume * self.master_volume)
		s.play()

	def play_loop(self, src, volume=1, tag=''):
		s = mixer.Sound(src)
		s.set_volume(volume * self.master_volume)

		if tag == '':
			if self.main_loop != None: self.stop_loop()
			self.main_loop = s
		else:
			if tag in self.tagged_loops: self.tagged_loops[tag].stop()
			self.tagged_loops[tag] = s
		s.play(-1)


	def stop_loop(self, tag=''):
		if tag == '':
			if self.main_loop != None:
				self.main_loop.stop()
				self.main_loop = None
		else:
			self.tagged_loops[tag].stop()
			del self.tagged_loops[tag]

	def stop_all_loops(self):
		for loop in self.tagged_loops:
			loop.stop()
		self.tagged_loops.clear()
		if self.main_loop != None:
			self.main_loop.stop()
			self.main_loop = None

