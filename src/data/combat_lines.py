

input_prompt = [
	'#Attack# or #block#?',
]

enemy_attacks = [
	'The @ strikes!',
	'The @ attacks!',
	'The @ swings!',
]

enemy_blocks = [
	'The @ is wary.',
	'The @ is vigilant.',
	'The @ is cautious.',
]

enemy_vulns = [
	'The @ stumbles!',
	'The @ hesitates!',
	'The @ is distracted!',
]


player_hits = [
	'The exile is wounded!',
	'The exile takes a hit!',
	'The exile is struck!',
]

player_strikes = [
	'The exile scores a hit!',
	'The exile\'s strike wounds the @!',
	'The exile\'s attack connects!',
]

player_blocks = [
	'The exile blocks the @\'s strike!',
	'The exile counters the attack!',
	'The exile dodges the @\'s swing!',
]

player_waits = [
	'The exile and the @ stare each other down.',
	'The opponents circle each other.',
	'The exile waits for the @ to strike.',
]

player_blocked = [
	'The exile swings and misses!',
	'The @ dodges the exile\'s strike!',
	'The @ blocks the exile\'s attack!',
]
