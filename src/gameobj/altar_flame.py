

from sprite import Sprite
from spark import Spark
from src.lib.scheduler import Scheduler
import random
import math


class AltarFlame(Sprite):

	def __init__(self, pos = [0,0]):
		fire_files = ['EXILE/art/vfx/fire_1.txt', 'EXILE/art/vfx/fire_2.txt', 'EXILE/art/vfx/fire_3.txt', 'EXILE/art/vfx/fire_4.txt', 'EXILE/art/vfx/fire_5.txt']
		super(AltarFlame, self).__init__(fire_files, pos, [0,0], 0)
		self.frame_scheduler = Scheduler(lambda: self.cycle_random_frames(), 400)
		self.spark_scheduler = Scheduler(lambda: self.gen_spark(), 2000)

	def update(self, ftime):
		super(AltarFlame, self).update(ftime)
		self.frame_scheduler.run(ftime)
		self.spark_scheduler.run(ftime)

	def cycle_frames(self):
		self.sprite_index += 1
		if self.sprite_index >= len(self.sprite_sheet):
			self.sprite_index = 0

	def cycle_random_frames(self):
		x = self.sprite_index
		while x == self.sprite_index:
			x = random.randrange(0,5)
		self.sprite_index = x

	def gen_spark(self):
		vel = .009
		p = [self.position['x'] + 2, self.position['y'] + 2]
		x, y = random.uniform(-1, 1), random.uniform(-1, 0)
		mag = math.sqrt(x*x + y*y)
		vec = [float(vel*x/mag), float(vel*y/mag)]
		Spark(text='.', pos=p, vel=vec, lifetime=1.0)
