

from particle import Particle
from src.core.manager import Manager
from src.lib.scheduler import Scheduler


class CombatBackground(object):

	def __init__(self):
		Manager.Instance().register_object(obj=self)
		self.particle_text = '====' #'------\n------'
		self.bkgd_counter = Scheduler(lambda: self.background_particles(), 1200)

	def update(self, ftime):
		self.bkgd_counter.run(ftime)

	def background_particles(self):
		for i in range(8):
			if i % 2 == 0:
				dir = 1
				orig = -10
			else:
				dir = -1
				orig = 82
			BackgroundParticle(self.particle_text, [orig, i*2], [dir*.015, 0])


class BackgroundParticle(Particle):

	def __init__(self, text='.', pos = [0,0], vel = [0,0]):
		super(BackgroundParticle, self).__init__(text, pos, vel, 0, 'back')

	def update(self, ftime):
		super(BackgroundParticle, self).update(ftime)
		self.check_bounds()

	def check_bounds(self):
		if self.position['x'] > 90 or self.position['x'] < -15:
			self.delete()
