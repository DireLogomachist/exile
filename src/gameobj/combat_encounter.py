

from fade import Fade, FadeColor
from scrolling_text import ScrollingText
from combat_background import CombatBackground
from turn_timer import TurnTimer
from health_bar import HealthBar
from strike import Strike
from src.data.combat_lines import *
from src.lib.scheduler import Scheduler
from src.core.manager import Manager
import random


class CombatEncounter(object):

	def __init__(self):
		Manager.Instance().clear_buffers()
		Manager.Instance().register_object(obj=self)
		Manager.Instance().level.fight_mode = True
		Manager.Instance().level.fight = self
		self.turn_time = 10000
		self.turn_timer = Scheduler(lambda: self.turn(timeout=True), self.turn_time)
		self.timer_sprite = TurnTimer([70,2])
		self.player_hp = 4
		self.enemy_hp = 3
		#self.bkgnd = CombatBackground()
		self.input_buffer = ''
		self.enemy_action = ''
		self.action_history = ['']
		self.paused = True
		self.health_bar = HealthBar('Enemy', self.enemy_hp, [61,2])
		
		Fade('in', 1.5)
		Manager.Instance().display.tk.after(2500, self.first_turn)


	def update(self, ftime):
		if not self.paused:
			self.turn_timer.run(ftime)
			self.timer_sprite.measure = 1.0 - float(self.turn_timer.timer/10000.0)


	def turn(self, timeout=False):
		# Reset timer
		self.turn_timer.reset()

		# Grab input
		if timeout:
			turn_input = 'timeout'
			Manager.Instance().level.dump_text()
		elif self.input_buffer in ['attack', 'block']:
			turn_input = self.input_buffer
		else:
			turn_input = 'timeout'
			Manager.Instance().level.dump_text()
		self.input_buffer = ''

		# Calculate turn result
		# Enemy Options: 'attack', 'block', 'timeout'
		# Player Options: 'attack', 'block', 'vuln'
		result = ''

		if turn_input == 'attack' and self.enemy_action == 'attack':
			# Get hit
			self.player_hp -= 1
			result = random.choice(player_hits)
			self.get_hit_fx()
		elif turn_input == 'attack' and self.enemy_action == 'block':
			# Enemy blocks
			result = random.choice(player_blocked)
			self.get_blocked_fx(result)
		elif turn_input == 'attack' and self.enemy_action == 'vuln':
			# Hit enemy
			self.enemy_hp -= 1
			self.health_bar.set_hp(self.enemy_hp)
			result = random.choice(player_strikes)
			self.hit_enemy_fx()
		elif turn_input == 'block' and self.enemy_action == 'attack':
			# Blocks enemy
			result = random.choice(player_blocks)
			self.block_enemy_fx(result)
		elif turn_input == 'block' and self.enemy_action == 'block':
			# Mutual wait
			result = random.choice(player_waits)
		elif turn_input == 'block' and self.enemy_action == 'vuln':
			# Mutual wait
			result = random.choice(player_waits)
		elif turn_input == 'timeout' and self.enemy_action == 'attack':
			# Get hit
			self.player_hp -= 1
			result = random.choice(player_hits)
			self.get_hit_fx()
		elif turn_input == 'timeout' and self.enemy_action == 'block':
			# Mutual wait
			result = random.choice(player_waits)
		elif turn_input == 'timeout' and self.enemy_action == 'vuln':
			# Mutual wait
			result = random.choice(player_waits)

		# Display turn results
		result = result.replace('@', self.health_bar.name.lower())
		ScrollingText(result)

		# Check for encounter end
		if self.enemy_hp <= 0:
			self.turn_timer.reset()
			self.paused = True
			Manager.Instance().level.fight_mode = False
			return

		# Select next enemy action
		if self.action_history[0] == self.action_history[1]:
			repeat_move = self.action_history[0]
		else:
			repeat_move = ''
		while self.enemy_action == repeat_move:
			self.enemy_action = random.choice(['attack', 'block', 'vuln', 'vuln'])

		if self.enemy_action == 'attack':
			action_prompt = random.choice(enemy_attacks)
		elif self.enemy_action == 'block':
			action_prompt = random.choice(enemy_blocks)
		elif self.enemy_action == 'vuln':
			action_prompt = random.choice(enemy_vulns)
		self.action_history.insert(0, self.enemy_action)


		# Prompt for imput
		action_prompt = action_prompt.replace('@', self.health_bar.name.lower())
		ScrollingText(action_prompt + '\n' + input_prompt[0], mode='input')


	def first_turn(self):
		self.paused = False
		self.enemy_action = 'attack'
		attack_prompt = random.choice(enemy_attacks)
		attack_prompt = attack_prompt.replace('@', self.health_bar.name.lower())
		ScrollingText(attack_prompt + '\n' + input_prompt[0], mode='input')
		self.action_history.insert(0, self.enemy_action)


	def hit_enemy_fx(self):
		# Animate slash
		Strike([25,2])
		# Sound effect
		sound = Manager.Instance().sound
		Manager.Instance().display.tk.after(100, lambda: sound.play('audio/interaction/combat_enemy_hit.wav', .3))

	def get_hit_fx(self):
		# Flash red
		Manager.Instance().display.canvas.configure(fg='#ff0000')
		Manager.Instance().display.tk.after(500, lambda: FadeColor(0.5, 'canvas', '#cccccc'))
		# Sound effect
		sound = Manager.Instance().sound
		sound.play('audio/interaction/combat_exile_hit.wav', .25)

	def block_enemy_fx(self, result):
		sound = Manager.Instance().sound
		if 'dodge' in result or 'misses' in result:
			Manager.Instance().display.tk.after(300, lambda: sound.play('audio/interaction/combat_exile_dodge.wav', .5))
		else:
			Manager.Instance().display.tk.after(300, lambda: sound.play('audio/interaction/combat_exile_clash.wav', .4))

	def get_blocked_fx(self, result):
		sound = Manager.Instance().sound
		if 'dodge' in result or 'misses' in result:
			Manager.Instance().display.tk.after(300, lambda: sound.play('audio/interaction/combat_enemy_dodge.wav', .6))
		else:
			Manager.Instance().display.tk.after(300, lambda: sound.play('audio/interaction/combat_enemy_clash.wav', .5))
