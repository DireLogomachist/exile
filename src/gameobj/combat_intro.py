

from src.core.manager import Manager
from sprite import Sprite
from spark import Spark
from fade import Fade


class CombatIntro(object):
	
	def __init__(self):
		Manager.Instance().register_object(obj=self)
		Manager.Instance().clear_buffers()
		Fade('in', 1.5)

		self.sword = Sprite(['EXILE/art/vfx/sword.txt'], pos = [-20,2])
		self.scabbard = Sprite(['EXILE/art/vfx/scabbard.txt'], pos = [-20,3])

		Manager.Instance().display.tk.after(2500, self.draw_particles)
		Manager.Instance().display.tk.after(3500, self.blade_draw)

	def update(self, ftime):
		pass

	def delete(self):
		Manager.Instance().remove_sprite(self.sword)
		Manager.Instance().remove_sprite(self.scabbard)
		Manager.Instance().remove_object(self)


	def draw_particles(self):
		spark1 = Spark('*', pos= [40, 4], vel=[ .02,-.02], lifetime=.3, layer='fore')
		spark2 = Spark('*', pos= [39, 4], vel=[-.02,-.02], lifetime=.3, layer='fore')
		spark3 = Spark('*', pos= [40,14], vel=[ .02, .02], lifetime=.3, layer='fore')
		spark4 = Spark('*', pos= [39,14], vel=[-.02, .02], lifetime=.3, layer='fore')
		Manager.Instance().sound.play('audio/interaction/sword_draw_ping.wav', 1)

	def blade_draw(self):
		self.sword.velocity = {'x': float(.003), 'y': float(0)}
		self.scabbard.velocity = {'x': float(-.04), 'y': float(0)}
		Manager.Instance().sound.play('audio/interaction/sword_draw.wav', 1)

		Manager.Instance().display.tk.after(700, self.reset_sword_vel)
		Manager.Instance().display.tk.after(2000, self.reset_scabbard_vel)
		Manager.Instance().display.tk.after(2000, lambda: Fade('out', 1.5))
		Manager.Instance().display.tk.after(3510, self.delete)

	def reset_sword_vel(self):
		self.sword.velocity = {'x': float(0), 'y': float(0)}

	def reset_scabbard_vel(self):
		self.scabbard.velocity = {'x': float(0), 'y': float(0)}
