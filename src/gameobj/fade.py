

from src.core.manager import Manager
from src.lib.conversions import convert_to_rgb, convert_to_hex
import time


class Fade(object):

	def __init__(self, mode = 'in', fade_time = 3, scope='canvas'):
		Manager.Instance().register_object(obj=self)

		self.t_start = time.clock()
		self.t_goal = fade_time
		self.mode = mode
		self.scope = scope

		if mode not in ['in', 'out']:
			raise Exception('Fade: Bad fade mode parameter \'' + mode + '\'')


	def update(self, ftime):
		t_progress = (time.clock() - self.t_start)/self.t_goal

		if t_progress >= 1:
			if self.mode == 'in':
				Manager.Instance().display.canvas.configure(fg=Manager.Instance().display.text_color)
				if self.scope == 'all':
					Manager.Instance().display.textbox.configure(fg=Manager.Instance().display.text_color)
					Manager.Instance().display.textdisplay.configure(fg=Manager.Instance().display.text_color)
			else:
				Manager.Instance().display.canvas.configure(fg='black')
				if self.scope == 'all':
					Manager.Instance().display.textbox.configure(fg='black')
					Manager.Instance().display.textdisplay.configure(fg='black')
			Manager.Instance().remove_object(self)

		c = int(round(t_progress * 204))
		if self.mode != 'in': c = 204 - c

		if c > 204: c = 204
		if c < 0: c = 0

		ct = convert_to_hex(c, c, c)
		Manager.Instance().display.canvas.configure(fg=ct)
		if self.scope == 'all':
			Manager.Instance().display.textbox.configure(fg=ct)
			Manager.Instance().display.textdisplay.configure(fg=ct)


class FadeColor(object):

	def __init__(self, fade_time=3, scope='canvas', color='#ff0000'):
		Manager.Instance().register_object(obj=self)

		self.t_start = time.clock()
		self.t_goal = fade_time
		self.scope = scope
		self.color = color
		self.orig_color = Manager.Instance().display.canvas.cget('fg')
		if self.orig_color == 'black':
			self.orig_color == '#000000'

	def update(self, ftime):
		t_progress = (time.clock() - self.t_start)/self.t_goal

		if t_progress >= 1:
			Manager.Instance().display.canvas.configure(fg=self.color)
			if self.scope == 'all':
				Manager.Instance().display.textbox.configure(fg=self.color)
				Manager.Instance().display.textdisplay.configure(fg=self.color)
			Manager.Instance().remove_object(self)

		origin = convert_to_rgb(self.orig_color)
		target = convert_to_rgb(self.color)
		
		r = min(max(0, int(round(t_progress * (target[0] - origin[0]) + origin[0]))), 255)
		g = min(max(0, int(round(t_progress * (target[1] - origin[1]) + origin[1]))), 255)
		b = min(max(0, int(round(t_progress * (target[2] - origin[2]) + origin[2]))), 255)

		ct = convert_to_hex(r, g, b)
		Manager.Instance().display.canvas.configure(fg=ct)
		if self.scope == 'all':
			Manager.Instance().display.textbox.configure(fg=ct)
			Manager.Instance().display.textdisplay.configure(fg=ct)
