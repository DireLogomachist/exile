

from combat_encounter import CombatEncounter
from sprite import Sprite


class GuardFight(CombatEncounter):

	def __init__(self):
		super(GuardFight, self).__init__()
		Sprite(['EXILE/art/character/guard_sword_drawn_alpha.txt'], pos = [32, 5])
		self.health_bar.name = 'Guard'

	def update(self, ftime):
		super(GuardFight, self).update(ftime)
