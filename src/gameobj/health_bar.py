

from sprite import Sprite


class HealthBar(Sprite):

	def __init__(self, name, hp, pos):
		super(HealthBar, self).__init__([], pos, [0,0], 0)
		self.name = name
		self.max_hp = hp
		self.hp = hp
		self.text = self.generate_text()

	def update(self, ftime):
		super(HealthBar, self).update(ftime)
		self.text = self.generate_text()

	def generate_text(self):
		text = '{:>6}\n'.format(self.name) + '-'*6 + '\nHP ' + str(self.hp) + '/' + str(self.max_hp)
		return text

	def set_hp(self, value):
		self.hp = value
