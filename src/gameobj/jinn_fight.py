

from combat_encounter import CombatEncounter
from sprite import Sprite
from spark import Spark
from miniremnant import MiniRemnant
from src.lib.scheduler import Scheduler
from src.core.manager import Manager
import math
import random


class JinnFight(CombatEncounter):

	def __init__(self):
		super(JinnFight, self).__init__()
		self.jinn_sprite = Sprite(['EXILE/art/character/jinn_alpha.txt'], pos = [32, 5])
		self.remnant_left = MiniRemnant([18,5])
		self.remnant_right = MiniRemnant([51,5])
		self.health_flag_1 = False
		self.health_flag_2 = False

		self.health_bar.name = 'Jinn'
		self.enemy_hp = 5
		self.health_bar.max_hp = self.enemy_hp
		self.health_bar.hp = self.enemy_hp

		self.vapor_scheduler = Scheduler(lambda: self.jinn_vapor(), 20)

	def update(self, ftime):
		super(JinnFight, self).update(ftime)
		self.vapor_scheduler.run(ftime)

		if self.health_flag_1 == False:
			if self.enemy_hp < 4:
				Manager.Instance().remove_sprite(self.remnant_left)
				self.health_flag_1 = True

		if self.health_flag_2 == False:
			if self.enemy_hp < 3:
				Manager.Instance().remove_sprite(self.remnant_right)
				self.health_flag_2 = True


	def jinn_vapor(self):
		vel = random.uniform(0,.004)
		phi = random.uniform(0, 2*math.pi)
		theta = math.acos(random.uniform(-1,1))
		x = math.sin(theta) * math.cos(phi)
		y = math.sin(theta) * math.sin(phi)
		unitpos = [self.jinn_sprite.position['x'] + 4 + 7*x, self.jinn_sprite.position['y'] + 3 + 4*y]

		Spark('.', unitpos, [0,vel], 1)
