

from sprite import Sprite
from spark import Spark
from src.lib.scheduler import Scheduler
import math
import random


class MiniRemnant(Sprite):

	def __init__(self, pos):
		self.origin = pos
		super(MiniRemnant, self).__init__(['EXILE/art/character/miniremnant.txt'], pos, [0,0], 'miniremnant')
		self.x_amp = 1.0
		self.y_amp = 1.0
		self.x_freq = 5.0
		self.y_freq = 5.0
		self.elapsed = 0.0
		self.vapor_scheduler = Scheduler(lambda: self.vapor(), 20)

	def update(self, ftime):
		self.hover(ftime)
		self.vapor_scheduler.run(ftime)
		super(MiniRemnant, self).update(ftime)


	def hover(self, ftime):
		self.elapsed += float(ftime)
		x = self.x_amp * math.sin(self.elapsed/1000.0 * (2.0 * math.pi) / self.x_freq) + self.origin[0]
		y = self.y_amp * math.cos(self.elapsed/1000.0 * (2.0 * math.pi) / self.y_freq) + self.origin[1]
		self.position = {'x': float(x), 'y': float(y)}

	def vapor(self):
		vel = random.uniform(0,.004)
		phi = random.uniform(0, 2*math.pi)
		theta = math.acos(random.uniform(-1,1))
		x = math.sin(theta) * math.cos(phi)
		y = math.sin(theta) * math.sin(phi)
		unitpos = [self.position['x'] + 1 + 2*x, self.position['y'] + y/2]

		Spark('.', unitpos, [0,vel], 1)
