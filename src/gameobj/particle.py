

from physics_object import PhysicsObject
from src.core.manager import Manager
from src.lib.conversions import to_line_list

class Particle(PhysicsObject):

	def __init__(self, text, pos = [0,0], vel = [0,0], ang_vel = 0, layer = 'back'):
		super(Particle, self).__init__(pos, vel, ang_vel)
		self.text = text
		self.c_width = Manager.Instance().display.canvas_width
		self.c_height = Manager.Instance().display.canvas_height
		self.layer = layer

		Manager.Instance().register_particle(particle=self)


	def update(self, ftime):
		super(Particle, self).update(ftime)

	def delete(self):
		Manager.Instance().remove_particle(self)
		

	def to_canvas_space(self):
		'Convert the particle to canvas space and return as string'

		text_lines = to_line_list(self.text)
		
		int_x = int(round(self.position['x']))
		int_y = int(round(self.position['y']))

		x_shift = ' ' * int_x
		y_shift = '\n' * int_y

		for i, line in enumerate(text_lines):
			if int_x >= 0:
				text_lines[i] = (x_shift + line)[:self.c_width]
			else:
				text_lines[i] = line[-int_x:self.c_width]
		text_lines = text_lines[:self.c_height]

		if int_y >= 0:
			canvas = y_shift + '\n'.join(text_lines)
		else:
			canvas = 'n'.join(text_lines[-int_y:])

		return canvas
