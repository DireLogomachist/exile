

from physics_object import PhysicsObject
from spark import Spark
from src.lib.scheduler import Scheduler
from src.core.manager import Manager
import math
import random


class ParticleCloud(PhysicsObject):

	def __init__(self, pos, rx, ry):
		Manager.Instance().register_object(obj=self)
		super(ParticleCloud, self).__init__(pos)
		self.rx = rx
		self.ry = ry
		self.tag = 'cloud'
		self.scheduler = Scheduler(lambda: self.particle_cloud(), 20)

	def update(self, ftime):
		self.scheduler.run(ftime)
	

	def particle_cloud(self):
		vel = random.uniform(0,.004)
		phi = random.uniform(0, 2*math.pi)
		theta = math.acos(random.uniform(-1,1))
		x = math.sin(theta) * math.cos(phi)
		y = math.sin(theta) * math.sin(phi)
		unitpos = [self.position['x'] + self.rx*x, self.position['y'] + self.ry*y]

		Spark('.', unitpos, [0,vel], 1)
