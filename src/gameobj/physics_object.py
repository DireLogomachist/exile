

class PhysicsObject(object):

	def __init__(self, pos = [0,0], vel = [0,0], ang_vel = 0):
		self.position = {'x': float(pos[0]), 'y': float(pos[1])}
		self.velocity = {'x': float(vel[0]), 'y': float(vel[1])}
		self.angular_velocity = float(ang_vel)


	def update(self, ftime):
		'Update position with velocity*ftime'

		self.position['x'] = self.position['x'] +  (self.velocity['x'] * float(ftime))
		self.position['y'] = self.position['y'] +  (self.velocity['y'] * float(ftime))
