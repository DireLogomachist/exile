

from particle import Particle
from src.core.manager import Manager
import random


class Raindrop(Particle):

	def __init__(self, text='.', pos = [0,0], vel = [0,0], sound=False):
		super(Raindrop, self).__init__(text, pos, vel, 0)
		self.sound = sound


	def update(self, ftime):
		super(Raindrop, self).update(ftime)
		self.check_hit_ground()

	def check_hit_ground(self):
		if self.position['y'] > self.c_height:
			self.text= '_' if random.randrange(0, 100) > 35 else '\_/'
			self.velocity = {'x': float(0), 'y': float(0)}
			self.position['y'] = float(self.c_height-1)
			if self.sound: self.hit_sound()
			Manager.Instance().display.tk.after(100, self.delete)

	def hit_sound(self):
		if bool(random.getrandbits(1)):
			Manager.Instance().sound.play('audio/environment/raindrop_1.wav', .3)
		else:
			Manager.Instance().sound.play('audio/environment/raindrop_2.wav', .3)
