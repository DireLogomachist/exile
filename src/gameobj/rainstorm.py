

from raindrop import Raindrop
from src.core.manager import Manager
from src.lib.scheduler import Scheduler
import random


class Rainstorm(object):

	def __init__(self):
		Manager.Instance().register_object(obj=self)
		self.rain_counter = Scheduler(lambda: Raindrop('|', [random.randrange(3, 77), -random.randrange(0,5)], [0, .03]), 40)


	def update(self, ftime):
		self.rain_counter.run(ftime)
