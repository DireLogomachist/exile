

from src.core.manager import Manager
from src.lib.scheduler import Scheduler


class ScrollingText(object):
	'Emulates scrolling text by returning an increasing partial string over time'

	def __init__(self, text, mode='enter', scroll_speed=50):
		self.full_text = text
		self.text = ''
		self.lifetime_cntr = 0
		self.scroll_speed = scroll_speed 	# In ms per char
		self.mode = mode
		self.updated = False
		self.complete = False
		self.textblip = Manager.Instance().sound.mixer.Sound('audio/interaction/text_blip.wav')
		self.blip_scheduler = Scheduler(lambda: self.playTextSound(), 100, False)

		Manager.Instance().level.flag_queue.append('wait ' + self.mode)
		Manager.Instance().register_text(self)


	def update(self, ftime):
		if self.complete: return
		self.lifetime_cntr += ftime

		newlen = self.lifetime_cntr // self.scroll_speed
		self.blip_scheduler.run(ftime)

		if newlen == len(self.text):
			self.updated = False
			return
		elif newlen >= len(self.text):
			self.text = self.full_text[:newlen]

			if newlen >= len(self.full_text) and self.complete == False:
				if self.mode == 'enter':
					Manager.Instance().display.enable_input_enter()
				elif self.mode == 'input':
					Manager.Instance().display.enable_input()
				self.complete = True
			self.updated = True
		else:
			raise Exception('Time continuum error. Please reboot universe.')

	def playTextSound(self):
		channel = Manager.Instance().sound.mixer.find_channel()
		channel.set_volume(.10)
		channel.play(self.textblip)

if __name__ == '__main__':
	import time

	st = ScrollingText('A blind man sits upon the bare earth', 80)
	wait_ms = 50

	while(True):
		st.update(wait_ms)
		if st.updated:
			print('\n'*50)
			print st.text

		if st.full_text == st.text:
			break
		time.sleep(wait_ms/float(1000))

