

from particle import Particle
from src.core.manager import Manager


class Spark(Particle):

	def __init__(self, text='*', pos = [0,0], vel = [0,0], lifetime=1, layer='back'):
		super(Spark, self).__init__(text, pos, vel, 0, layer)
		self.lifetime = lifetime

		Manager.Instance().display.tk.after(int(lifetime*1000), self.delete)


	def update(self, ftime):
		super(Spark, self).update(ftime)
