

from physics_object import PhysicsObject
from src.core.manager import Manager
from src.lib.conversions import to_line_list

class Sprite(PhysicsObject):

	def __init__(self, files, pos = [0,0], vel = [0,0], tag = ''):
		super(Sprite, self).__init__(pos, vel, 0)
		self.sprite_index = 0
		self.sprite_sheet = []
		self.c_width = Manager.Instance().display.canvas_width
		self.c_height = Manager.Instance().display.canvas_height
		self.tag = tag
		self.enabled = True

		for file in files:
			with open('../' + file) as f:
				self.sprite_sheet.append(f.read())

		if len(self.sprite_sheet) == 0:
			self.sprite_sheet = ['']

		Manager.Instance().register_sprite(sprite=self)


	def update(self, ftime):
		super(Sprite, self).update(ftime)


	@property
	def text(self):
		return self.sprite_sheet[self.sprite_index]

	@text.setter
	def text(self, value):
		self.sprite_sheet[self.sprite_index] = value


	def enable(self): self.enabled = True
	def disable(self): self.enabled = False

	def next_sprite_frame(self):
		self.sprite_index += 1
		if self.sprite_index >= len(self.sprite_sheet):
			self.sprite_index = 0

	def to_canvas_space(self):
		'Convert the sprite to canvas space and return as string'

		if self.enabled == False: return ''
		
		text_lines = to_line_list(self.sprite_sheet[self.sprite_index])
		
		int_x = int(round(self.position['x']))
		int_y = int(round(self.position['y']))

		x_shift = ' ' * int_x
		y_shift = '\n' * int_y

		for i, line in enumerate(text_lines):
			if int_x >= 0:
				text_lines[i] = (x_shift + line)[:self.c_width]
			else:
				text_lines[i] = line[-int_x:]
				text_lines[i] = text_lines[i][:self.c_width]

		if int_y >= 0:
			canvas = y_shift + '\n'.join(text_lines)
		else:
			text_lines = text_lines[-int_y:]
			canvas = '\n'.join(text_lines[:self.c_height])

		return canvas
