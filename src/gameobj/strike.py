

from sprite import Sprite
from spark import Spark
from src.core.manager import Manager


class Strike(Sprite):
	def __init__(self, pos):
		files= ['EXILE/art/vfx/strike_1.txt', 'EXILE/art/vfx/strike_2.txt', 'EXILE/art/vfx/strike_3.txt']
		super(Strike, self).__init__(files, pos, [0,0], 0)
		tk = Manager.Instance().display.tk
		self.sprite_index = 1
		tk.after(200, lambda: self.set_index(2))
		tk.after(200, lambda: Spark('1', [pos[0]+21,pos[1]+4], [0,.006], .5, 'fore'))
		tk.after(400, lambda: Manager.Instance().remove_sprite(self))

	def update(self, ftime):
		super(Strike, self).update(ftime)

	def set_index(self, num):
		self.sprite_index = num
