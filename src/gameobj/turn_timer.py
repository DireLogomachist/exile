

from sprite import Sprite
from src.lib.conversions import sprite_mask
from src.core.manager import Manager


class TurnTimer(Sprite):

	def __init__(self, pos):
		super(TurnTimer, self).__init__(['EXILE/art/ui/turn_timer.txt'], pos, [0,0], 0)
		self.offset = [2,0]
		self.measure = 1.0

	def update(self, ftime):
		super(TurnTimer, self).update(ftime)
		x = int(round(self.measure*5))
		new_text = sprite_mask(self.text, '\n  A'*(5-x) + '\n  #'*x)
		self.text = new_text

	def set_timer(self, measure):
		self.measure = measure
