''' Conversion functions
'''

from itertools import izip_longest

def to_2d_array(text):
	array = [list(x) for x in text.split('\n')]
	return array


def from_2d_array(array):
	text = '\n'.join([''.join(sublist) for sublist in array])
	return text


def to_line_list(text):
	lines = text.split('\n')
	return lines


def sprite_mask(text, mask):
	text_lines = to_line_list(text)
	mask_lines = to_line_list(mask)

	new_sprite = '\n'.join(line_mask(line, lmask) for line, lmask in izip_longest(text_lines, mask_lines, fillvalue=[]))
	return new_sprite


def line_mask(line, lmask):
	new_line = "".join(x if y == ' ' else y for x, y in izip_longest(line, lmask, fillvalue=' '))
	return new_line


def gen_pair(l):
		for i in range(0, len(l), 2):
			yield l[i:i + 2]

def convert_to_rgb(color):
	return tuple(int(color.lstrip('#')[i:i+2], 16) for i in (0, 2 ,4))

def convert_to_hex(r, g, b):
	return '#%02x%02x%02x' % (r, g, b)


def count_preceding_lines(text, index):
	return len(text[:index].split('\n'))

def count_preceding_line_chars(text, index):
	return sum(len(s) for s in text[:index].split('\n')[:-1])
