

class Scheduler(object):

	def __init__(self, function, rate, catchup=True):
		self.function = function
		self.rate = rate
		self.timer = 0
		self.catchup = catchup
		self.paused = False

	def run(self, ftime):
		if not self.paused:
			self.timer += ftime

		if self.timer > self.rate:
			if self.catchup:
				for x in xrange(int(self.timer / self.rate)):
					self.function()
			else:
				self.function()
			self.timer = self.timer - self.rate * (self.timer / self.rate)

	def reset(self):
		self.timer = 0

	def start(self):
		self.paused = False

	def pause(self):
		self.paused = True
