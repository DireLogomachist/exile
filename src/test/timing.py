

import time
from itertools import izip_longest
from conversions import to_line_list, sprite_mask, line_mask
from sprite import Sprite


if __name__ == '__main__':
	
	s = Sprite(['art/vfx/eye_plain.txt'], [5,5])
	m = Sprite(['art/vfx/eye.txt'], [0,8])

	# Canvas space conversion timing
	start  = time.time()
	runs = 1000
	for x in range(runs):
		text = s.to_canvas_space()
	end = time.time()
	print 'canvas space timing: ' + str((end-start)*1000/runs) + ' ms'

	# Sprite mask timing
	start  = time.time()
	runs = 1000
	for x in range(runs):
		text = sprite_mask(s.to_canvas_space(), m.to_canvas_space())
	end = time.time()
	print 'sprite mask: ' + str((end-start)*1000/runs) + ' ms'
	
	# Line list conversion timing
	start  = time.time()
	runs = 1000
	for x in range(runs):
		text_lines = to_line_list(s.text)
	end = time.time()
	print 'line list comprehension: ' + str((end-start)*1000/runs) + ' ms'

	# izip sprite mask timing
	text_lines = to_line_list(s.text)
	mask_lines = to_line_list(m.text)

	start  = time.time()
	runs = 1000
	for x in range(runs):
		new_sprite = '\n'.join(line_mask(line, lmask) for line, lmask in izip_longest(text_lines, mask_lines, fillvalue=[]))
	end = time.time()
	print 'izip sprite comprehension: ' + str((end-start)*1000/runs) + ' ms'

	# izip sprite mask timing
	line = to_line_list(s.text)[0]
	lmask = to_line_list(m.text)[0]

	start  = time.time()
	runs = 1000
	for x in range(runs):
		new_line = "".join(x if y == ' ' else y for x, y in izip_longest(line, lmask, fillvalue=' '))
	end = time.time()
	print 'izip line comprehension: ' + str((end-start)*1000/runs) + ' ms'
